Roemer Softworks
================

This is my homepage. Originally set up as a landing page for my Android 
application, it has become the home for my personal contact and other
information. It is intentionally simple, and can be viewed correctly with *any*
browser (CLI, graphical, mobile, desktop, Internet of Vulnerable Things), in an
effort to combat the needless bloat that has permeated the web.

To this end, this site offers its content with nothing but confirmed valid HTML
and CSS.

- https://validator.w3.org/nu/?showsource=yes&doc=http://roemersoftworks.com/
- http://jigsaw.w3.org/css-validator/validator?uri=www.roemersoftworks.com&profile=css3&usermedium=all&warning=1&vextwarning=

It also receives a score of 99% from PageSpeed, and 100% from YSlow.

- https://gtmetrix.com/reports/www.roemersoftworks.com/Sgp8Yhr9

The source of this page is both libre and gratis under the terms of the GPLv3.
